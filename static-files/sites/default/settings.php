<?php

$conf['drupalorg_site'] = 'groups';
$conf['logintoboggan_pre_auth_role'] = 33;
$conf['drupalorg_crosssite_trusted_role'] = 13;

// Bakery settings.
$conf['bakery_is_master'] = FALSE;
$conf['bakery_allow_local'] = FALSE;

$conf += array(
  'apachesolr_site_hash' => 'groups',
  'drupalorg_site' => 'groups',
  'drupalorg_base_domain' => 'drupal.org',
);

/**
 * Include a common settings file if it exists.
 */
$common_settings = '/var/www/settings.common.php';
if (file_exists($common_settings)) {
  include $common_settings;
}

/**
 * Include a local settings file if it exists.
 */
$local_settings = dirname(__FILE__) . '/settings.local.php';
if (file_exists($local_settings)) {
  include $local_settings;
}
