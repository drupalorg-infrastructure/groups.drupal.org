core = 6.x
api = 2
projects[drupal][version] = "6.38"

; Tag1 Quo D6 LTS
projects[tag1quo][version] = "1.1"

; Probably mostly PressFlow patches.
projects[drupal][patch][] = "https://bitbucket.org/drupalorg-infrastructure/groups.drupal.org/raw/6.x-prod/patches/drupal.diff"

; Add PHP strict error checking for taxonomy.
; See https://www.drupal.org/node/996124 for more info.
projects[drupal][patch][] = "https://www.drupal.org/files/issues/taxonomy.pages_.inc__5.patch"

; https://www.drupal.org/SA-CORE-2016-003
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p1.patch"

; https://www.drupal.org/sa-core-2018-001
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p2.patch"

; https://www.drupal.org/sa-core-2018-002
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p3.patch"
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p4.patch"

; https://www.drupal.org/sa-core-2018-004
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p5.patch"

; https://www.drupal.org/sa-core-2018-006
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p6.patch"

; https://www.drupal.org/sa-core-2019-002
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p7.patch"

; https://www.drupal.org/sa-core-2019-006
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p8.patch"

; https://www.drupal.org/sa-core-2019-007
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p9.patch"

; https://www.drupal.org/sa-core-2020-002
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p10.patch"

; https://www.drupal.org/sa-core-2020-004
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p11.patch"

; https://www.drupal.org/sa-core-2020-007
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p12.patch"

; https://www.drupal.org/sa-core-2020-012
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p13.patch"

; https://www.drupal.org/sa-core-2021-002
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p14.patch"

; https://www.drupal.org/sa-core-2022-003
projects[drupal][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/core/drupal-6.38-p15.patch"

; Modules
projects[apachesolr][version] = "3.1"
projects[apachesolr_multisitesearch][version] = "3.x-dev"
projects[apachesolr_multisitesearch][download][revision] = "981d83a"
projects[apachesolr_og][version] = "3.0"
projects[apachesolr_url_switch][version] = "1.x-dev"
projects[apachesolr_url_switch][download][revision] = "fa494ce"
projects[auto_nodetitle][version] = "1.x-dev"
projects[auto_nodetitle][download][revision] = "0f9e97c"

projects[bakery][version] = "2.x-dev"
projects[bakery][download][revision] = "e4665c7"
; Remove rebaking the cookie on subsites, which removes the master switch.
; https://www.drupal.org/node/1450842
projects[bakery][patch][] = "https://www.drupal.org/files/issues/1450842-bakery-sso-cookie-master-5-d6.patch"
; Redirect to master a password reset request from a slave
; https://www.drupal.org/node/1967936#comment-7478158
projects[bakery][patch][] = "https://www.drupal.org/files/1967936-d6.patch"

projects[better_exposed_filters][version] = "2.x-dev"
projects[better_exposed_filters][download][revision] = "a842c6e"

projects[calendar][version] = "2.4"
; Cleanup to support PHP 5.4
; See https://www.drupal.org/node/999514 and https://www.drupal.org/node/2458331 for more info.
projects[calendar][patch][] = "https://www.drupal.org/files/issues/_calendar-999514-6-D6-2.x.patch.txt"
projects[calendar][patch][] = "https://www.drupal.org/files/issues/calendar-n2458331-1.patch"

projects[cck][version] = "2.10"
; Cleanup to support PHP 5.4
; See https://www.drupal.org/node/2327005 for more info.
projects[cck][patch][] = "https://www.drupal.org/files/issues/cck-e_strict-2327005-26.patch"
; References - Unsupported - SA-CONTRIB-2017-38
; https://www.drupal.org/node/2869138
projects[cck][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/contrib/cck/cck-6.x-2.10-p1.patch"

projects[codefilter][version] = "1.0"

projects[ctools][version] = "1.15"
; https://www.drupal.org/sa-core-2020-007
projects[ctools][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/contrib/ctools/ctools-6.x-1.15-p1.patch"

projects[date][version] = "2.x-dev"
projects[date][download][revision] = "f2bc02e4"
; PHP5.4 issue: warning: Illegal string offset 'date'
; See https://www.drupal.org/node/2004508#comment-10867810 for more info.
;projects[date][patch][] = "https://www.drupal.org/files/issues/date_popup-2004508-illegal-offset-13.patch"

projects[diff][version] = "2.3"

projects[drupalorg_crosssite][version] = "3.x-dev"
projects[drupalorg_crosssite][download][revision] = "17a2a5d"

projects[facetapi][version] = "3.0-beta2"
projects[fasttoggle][version] = "1.6"

projects[features][version] = "1.2"
projects[features][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/contrib/features/features-6.x-1.2-p1.patch"

projects[filter_html_image_secure][version] = "1.0"

projects[flag][version] = "2.2"
projects[flag][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/contrib/flag/flag-6.x-2.2-p1.patch"

; Freelinking is stuck at 6.x-1.x until the 7.x upgrade. The Security issues in
; later versions of 6.x do not affect the 1.x branch.
;projects[freelinking][version] = "1.x-dev"
projects[freelinking][download][revision] = "f626cf3"

projects[google_analytics][version] = "4.3"

projects[groupsdrupalorg][version] = "1.x-dev"
projects[groupsdrupalorg][download][revision] = "75abec3"

projects[honeypot][version] = "1.19"
projects[image][version] = "1.2"
projects[jquery_ui][version] = "1.5"

projects[l10n_update][version] = "1.0-beta4"

projects[logintoboggan][version] = "1.11"
projects[markdown][version] = "1.4"

projects[messaging][version] = "2.x-dev"
projects[messaging][download][revision] = "4cb5f83"
; sender_name should be used when preparing mail
; https://www.drupal.org/node/1067198#comment-5392676
projects[messaging][patch][] = "https://www.drupal.org/files/sender_name.patch"

projects[notifications][version] = "2.3"
projects[og][version] = "2.x-dev"
projects[og][download][revision] = "cb50af6"
; Cleanup to support PHP 5.4
; See https://www.drupal.org/node/2669444#comment-10859728 for more info.
projects[og][patch][] = "https://www.drupal.org/files/issues/2669444-og-views-uid-groups-compatible-2.patch"

projects[og_panels][version] = "2.x-dev"
projects[og_panels][download][revision] = "99f888a"
projects[og_vocab][version] = "1.3"
; Cleanup to support PHP 5.4
; See https://www.drupal.org/node/2669434#comment-10859708 for more info.
projects[og_vocab][patch][] = "https://www.drupal.org/files/issues/2669434-og_vocab_pass_by_reference-1.patch"

projects[panels][version] = "3.12"
projects[panels][patch][] = "https://raw.githubusercontent.com/tag1consulting/lts-patches/6.x/contrib/panels/panels-6.x-3.12-p1.patch"

projects[paranoia][version] = "1.2"
projects[path_redirect][version] = "1.0-rc2"
projects[pathauto][version] = "1.6"

projects[phpass][version] = "2.2"

projects[r4032login][version] = "1.6"
projects[radioactivity][version] = "1.4"
projects[security_review][version] = "1.x-dev"
projects[security_review][download][revision] = "8bf4535"
projects[signup][version] = "1.x-dev"
projects[signup][download][revision] = "c6d407f"
; Cleanup to support PHP 5.4
; See https://www.drupal.org/node/2599698 for more info.
projects[signup][patch][] = "https://www.drupal.org/files/issues/signup-static-argument-pre-render-method-2599698-3.patch"

projects[token][version] = "1.19"
projects[usermerge][version] = "1.x-dev"
projects[usermerge][download][revision] = "60c2537"

; Fake version number to line up with Quo.
projects[views][version] = "2.18-p5"
projects[views][download][revision] = "d62a5f1"
; Views - Moderately Critical - Access Bypass - SA-CONTRIB-2017-022
; https://www.drupal.org/node/2854980
projects[views][patch][] = "https://bitbucket.org/drupalorg-infrastructure/groups.drupal.org/raw/6.x-prod/patches/views-6.x-2.18-p3-drupalorg.patch"
projects[views][patch][] = "https://bitbucket.org/drupalorg-infrastructure/groups.drupal.org/raw/6.x-prod/patches/views-6.x-2.18-p4-drupalorg.patch"
projects[views][patch][] = "https://bitbucket.org/drupalorg-infrastructure/groups.drupal.org/raw/6.x-prod/patches/views-6.x-2.18-p5-drupalorg.patch"
; Strict warning: Creating default object from empty value
; See https://www.drupal.org/node/1156198#comment-10866856 for more info.
projects[views][patch][] = "https://www.drupal.org/files/issues/1156198-views-comment_username-empty-object-5.patch"

projects[views_bulk_operations][version] = "1.17"
projects[vote_up_down][version] = "3.2"
; Cleanup to support PHP 5.4
; See https://www.drupal.org/node/2669438#comment-10859722 for more info.
projects[vote_up_down][patch][] = "https://www.drupal.org/files/issues/2669438-vud-php-notices-1.patch"

projects[votingapi][version] = "2.3"


; Libraries
libraries[jquery_ui][destination] = "libraries"
libraries[jquery_ui][directory_name] = "jquery.ui"
libraries[jquery_ui][download][type] = "get"
libraries[jquery_ui][download][url] = "http://jqueryui.com/resources/download/jquery-ui-1.6.zip"


; Theme
projects[bluecheese][type] = "theme"
projects[bluecheese][download][type] = "git"
projects[bluecheese][download][url] = "git@bitbucket.org:/drupalorg-infrastructure/bluecheese-private.git"
projects[bluecheese][download][branch] = "6.x-branded"
